import ApiBaseClass from '../baseClass';
import { ApiConstants as CP } from './constants';

class Dispatch extends ApiBaseClass {

  async list(data) {
    return await this.req('post', CP.dispatch.list, data)
  }
  async detail(id) {
    return await this.req('get', CP.dispatch.detail(id))
  }

}

export default new Dispatch();
