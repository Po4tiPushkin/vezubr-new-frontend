import { Ant, FilterButton } from "@vezubr/elements";
import { Popover } from "antd";
import React, { useMemo } from "react";

const CLS = "choose-app";
const ROLES = [
  ["dispatcher", "Экспедитор"],
  ["client", "Грузовладелец"],
  ["producer", "Перевозчик"],
  ["operator", "Оператор"],
];

function ChooseApp() {
  const [visible, setVisible] = React.useState(false);

  const onChoose = (newApp) => {
    if (window.APP !== newApp) {
      window.LOCAL_APP = newApp;
      localStorage.setItem('newApp', newApp)
      window.location.reload();
    }
  };

  const renderRoles = useMemo(
    () =>
      ROLES.map(([key, text]) => {
        return (
          <Ant.Button
            key={key}
            className={`${CLS}__item`}
            onClick={() => onChoose(key)}
            disabled={window.APP == key}
          >
            {text}
          </Ant.Button>
        );
      }),
    [ROLES]
  );


  return (
    <Popover
      content={<div className={`${CLS}__form`}>{renderRoles}</div>}
      title="Выбор приложения"
      trigger="click"
      visible={visible}
      onVisibleChange={() => setVisible((prev) => !prev)}
    >
      <FilterButton wrapperClassName={CLS} icon={"regularOrders"} className={'circle'}></FilterButton>
    </Popover>
  );
}

export default ChooseApp;
