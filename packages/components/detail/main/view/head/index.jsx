import React, { useMemo } from 'react';
import { useHistory } from 'react-router';
import { LinkGoBackRenderProps } from '@vezubr/components';

const DetailMainHead = () => {
  const history = useHistory();
  const { location } = history;
  const backUrl = useMemo(() => (location.state ? location?.state?.back?.pathname : '/orders'), [order?.id]);
  const goBackRender = useMemo(() => {
    return (
      <LinkGoBackRenderProps location={location} defaultUrl={'/orders'}>
        {() => <IconDeprecated name={'backArrowOrange'} className={'pointer'} onClick={() => history.push(backUrl)} />}
      </LinkGoBackRenderProps>
    );
  }, [location, history]);
  return (
    <div className='detail-main-view-head'>
      <div className="detail-main-view-head__top">
        <div className="detail-view-title">
          <span className='view-id'></span>
          <span>

          </span>
        </div>
        <div className="detail-view-actions">
          <div className="flexbox">

          </div>
        </div>
      </div>
      <div className="detail-view-head-bottom">
        
      </div>
    </div>
  )
}

export default DetailMainHead;