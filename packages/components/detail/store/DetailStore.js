import { observable, computed, action, runInAction } from 'mobx';
import DetailData from "./DetailData";
import DetailHead from "./view/head/DetailHead";
import DetailLeft from "./view/left/DetailLeft";
import DetailRight from "./view/right/DetailRight";

class DetailStore {
  _data = {};
  _head = {};
  _right = {};
  _left = {};
  _dictionaries = {};
  _constants = {};
  constructor(payload) {
    this._init(payload);
  }

  @action
  _init({ dictionaries = {}, constants = {}, data = {} }) {
    this._dictionaries = dictionaries;
    this._constants = constants;
    this._data = data;
    this._head = new DetailHead(this);
    this._left = new DetailLeft(this);
    this._right = new DetailRight(this);

  }
}

export default DetailStore;