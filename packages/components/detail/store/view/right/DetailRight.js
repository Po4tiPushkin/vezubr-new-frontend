import DetailRouter from "../../router/DetailRouter";

// abstract
class DetailRight {
  _dataStore = {};
  _router = {}
  constructor(dataStore) {
    this._dataStore = dataStore;
    this._init();
  }

  setRouter() {
    this._router = new DetailRouter([]);
  }
  _init() {
    this.setRouter()
  }
}

export default DetailRight;