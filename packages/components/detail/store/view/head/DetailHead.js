import { observable, computed, action, runInAction } from 'mobx';

class DetailHead {
  _dataStore = {};
  _menu = {};
  _title = '';
  constructor(dataStore) {
    this._dataStore = dataStore;
    this._init();
  }

  
  _init() {

  }

  getTitle() {
    return '';
  }

  getMenu() {
    return [];
  }
}

export default DetailHead;