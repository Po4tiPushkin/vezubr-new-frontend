import { observable, computed, action, runInAction } from 'mobx';

class DetailHeadMenuItem {
  _title = '';
  _icon = '';
  _id = '';
  _action = () => {};
  disabled = false;

  constructor(title, icon, id, action, disabled) {
    this._title = title;
    this._icon = icon;
    this._id = id;
    this._action = action;
    this._disabled = disabled;
  }

  onAction() {
    this._action();
  }

}

export default DetailHeadMenuItem;