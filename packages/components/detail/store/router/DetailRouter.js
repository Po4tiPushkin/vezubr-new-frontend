import { Route, Switch } from 'react-router';
import { observable, computed, action, runInAction } from 'mobx';
import DetailRoute from './DetailRoute';

class DetailRouter {
  _routes = [];

  constructor(routeArray) {
    if (Array.isArray(routeArray)) {
      this.setRoutes(routeArray);
    }
  }

  @computed get routes() {
    return this._routes;
  }

  @action
  setRoutes(routeArray = []) {
    _routes = routeArray.map(el => {
      return new DetailRoute(el);
    })
  }
}

export default DetailRouter;