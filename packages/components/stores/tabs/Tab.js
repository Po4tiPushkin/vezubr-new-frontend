import { observable, computed, action, runInAction } from 'mobx';

class Tab {
  @observable _show = true;
  @observable _id = '';
  @observable _title = '';
  @observable _route = '';
  @observable _disabled = false;

  constructor({ show, id, title, route, disabled }) {
    this._id = id;
    this._show = show;
    this._title = title;
    this._route = route;
    this._disabled = disabled;
  }

  @computed get show() {
    return this._show;
  }
  @computed get id() {
    return this._id;
  }
  @computed get title() {
    return this._title;
  }
  @computed get route() {
    return this._route;
  }
  @computed get disabled() {
    return this._disabled;
  }
}

export default Tab;