import { TabConstants } from "../constants"

const loadingUnloadingTrnSign = (document) => {

}


const getCanSignTrn = (tab, document = {}) => {
  switch (tab) {
    case TabConstants.LOADING_TAB:
    case TabConstants.UNLOADING_TAB:
      return loadingUnloadingTrnSign(document);
    default:
      return false
  }
}

const getCanSign = (tab, document = {}) => {
  switch (document.documentType) {
    case 'trn':
      return getCanSignTrn(tab, document);
    default:
    return false;
  }
}