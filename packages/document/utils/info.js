import moment from 'moment';

const getTrnExtractInfo = (document, dictionaries) => {
  return [
    {
      title: 'Тип Подписания ',
      value: document.electronicDocFlow ? 'Безбумажный' : 'Бумажный'
    },
    {
      title: 'К Рейсу №',
      value: document.orderNr
    },
    {
      title: 'К Заявке №',
      value: document.requestNr
    },
    {
      title: 'Дата создания документа',
      value: document.createdAt ? moment(document.createdAt).format('DD MMMM, YYYY HH:mm') : ''
    },
  ]
}

export const getExtractInfo = (document, dictionaries) => {
  switch (document.documentType) {
    case 'trn':
      return getTrnExtractInfo(document, dictionaries);
    default:
      return [];
  }
}