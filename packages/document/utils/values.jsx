import { TabConstants } from "../constants"

const orderInfoValues = (document) => {
  const { cargo = {} } = document;
  return [{
    cargoType: cargo.packagingTypeTitle,
    weight: cargo.weight / 1000,
    countCargoPlaces: cargo.countCargoPlaces,
    volume: cargo.volume,
    cargoCategory: cargo.categoryTitle,
    assessedCargoValue: cargo.assessedCargoValue / 100,
    arrivalDate: document.toStartAt
  }, {}]
}

const getTrnLoadingUnloadingValues = (tabInfo, document) => {
  switch (tabInfo.block) {
    case 'orderInfo':
      return orderInfoValues(document);
    default:
      return [{}, {}]
  }
}

const getTrnValuesByTab = (tabInfo, document) => {
  switch (tabInfo.tab) {
    case TabConstants.LOADING_TAB:
    case TabConstants.UNLOADING_TAB:
      return getTrnLoadingUnloadingValues(tabInfo, document)
    default:
      return [{}, {}]
  }
}

export const getValues = (tabInfo = {}, document = {}) => {
  switch (document.documentType) {
    case 'trn':
      return getTrnValuesByTab(tabInfo, document)
    default:
      return [{}, {}]
  }
}