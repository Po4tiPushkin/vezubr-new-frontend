import React, { useContext, useMemo } from "react";
import {
  StatusNotificationNew,
} from '@vezubr/components';


const DocumentViewProblem = (props) => {
  const problem = useMemo(() => {
  }, [])
  return (
    <>
      {problem ? (
        <StatusNotificationNew
          type={'info'}
          actions={problem.actions}
          title={problem?.title}
          color={'red'}
          description={problem?.description}
        />
      ) : null}
    </>
  )
}

export default DocumentViewProblem;