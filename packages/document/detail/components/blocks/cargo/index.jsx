import React, { useCallback, useState, useMemo, useEffect, useContext } from 'react';
import { Ant, VzForm, } from '@vezubr/elements';
import moment from 'moment';
import { useSelector } from 'react-redux';
import DocumentViewContext from '../../../context';
const FIELDS = {
  cargoType: 'cargoType',
  countCargoPlaces: 'countCargoPlaces',
  weight: 'weight',
  volume: 'volume',
  assessedCargoValue: 'assessedCargoValue',
  arrivalDate: 'arrivalDate',
  cargoCategory: 'cargoCategory',

}

const CargoBlock = (props) => {
  const { values, canEdit, canSign, title, form, extraFields } = props;
  const { actions } = useContext(DocumentViewContext);
  const [editing, setEditing] = useState(false);
  const dictionaries = useSelector(state => state.dictionaries);
  const { getFieldError, getFieldDecorator, getFieldValue, getFieldsValue, setFieldsValue } = form;

  return (
    <div className="document-view-block">
      <VzForm.Group title={title}>
        <VzForm.Item
          disabled={!editing}
          label={'Тип Груза'}
          error={getFieldError(FIELDS.cargoType)}
        >
          {getFieldDecorator(FIELDS.cargoType, {
            initialValue: values?.[FIELDS.cargoType] || null,
            // rules: rules[FIELDS.cargoCategory](getFieldsValue()),
          })(
            <Ant.Input disabled={!editing} />,
          )}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Вес Груза, кг'}
          error={getFieldError(FIELDS.weight)}
        >
          {getFieldDecorator(FIELDS.weight, {
            initialValue: values?.[FIELDS.weight] || null,
            // rules: rules[FIELDS.weight](getFieldsValue()),
          })(<Ant.InputNumber disabled={!editing} placeholder={''} />)}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Количество Груза, шт'}
          error={getFieldError(FIELDS.countCargoPlaces)}
        >
          {getFieldDecorator(FIELDS.countCargoPlaces, {
            initialValue: values?.[FIELDS.countCargoPlaces] || null,
            // rules: rules[FIELDS.countCargoPlaces](getFieldsValue()),
          })(<Ant.InputNumber disabled={!editing} placeholder={''} />)}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Объем Груза, м3'}
          error={getFieldError(FIELDS.volume)}
        >
          {getFieldDecorator(FIELDS.volume, {
            initialValue: values?.[FIELDS.volume] || null,
            // rules: rules[FIELDS.volume](getFieldsValue()),
          })(<Ant.InputNumber disabled={!editing} placeholder={''} />)}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Категория Груза'}
          error={getFieldError(FIELDS.cargoCategory)}
        >
          {getFieldDecorator(FIELDS.cargoCategory, {
            initialValue: values?.[FIELDS.cargoCategory] || null,
            // rules: rules[FIELDS.cargoCategory](getFieldsValue()),
          })(
            <Ant.Input disabled={!editing} />,
          )}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Стоимость Груза, руб'}
          error={getFieldError(FIELDS.assessedCargoValue)}
        >
          {getFieldDecorator(FIELDS.assessedCargoValue, {
            initialValue: values?.[FIELDS.assessedCargoValue] || null,
            // rules: rules[FIELDS.assessedCargoValue](getFieldsValue()),
          })(<Ant.InputNumber disabled={!editing} placeholder={''} />)}
        </VzForm.Item>
        <VzForm.Item
          disabled={!editing}
          label={'Требуемое время прибытия'}
          error={getFieldError(FIELDS.arrivalDate)}
        >
          {getFieldDecorator(FIELDS.arrivalDate, {
            // rules: rules[FIELDS.arrivalDate](getFieldsValue()),
            initialValue: moment(values?.[FIELDS.arrivalDate]).isValid() ? moment(values?.[FIELDS.arrivalDate]) : null
          })(<Ant.DatePicker showTime disabled={!editing} />)}
        </VzForm.Item>
        {
          canSign && (
            <div style={{ alignSelf: 'center' }} className="">
              <Ant.Button onClick={() => actions.onSign()}>
                Редактировать
              </Ant.Button>
            </div>
          )
        }
        {canEdit && (
          <div style={{ alignSelf: 'center' }} className="">
            <Ant.Button onClick={() => setEditing(true)}>
              Редактировать
            </Ant.Button>
          </div>
        )
        }
      </VzForm.Group>
    </div>
  )
}

export default Ant.Form.create({})(CargoBlock);
