import { useSelector } from "react-redux";
import React, { useMemo, useContext } from 'react';
import Utils from '@vezubr/common/common/utils';
import _get from 'lodash/get';
import { useHistory } from "react-router";
import * as InfoUtils from '../../../utils/info';
import { OrderSidebarInfos } from '@vezubr/components';
import DocumentViewContext from "../../context";
const DocumentViewTopInfo = (props) => {
  const { document } = useContext(DocumentViewContext);
  const dictionaries = useSelector(state => state.dictionaries);
  const topInfo = useMemo(() => {
    return InfoUtils.getExtractInfo(document, dictionaries)
  }, [document.documentType])
  return (
    <OrderSidebarInfos data={topInfo.filter(el => el && el.value)} />
  )
}

export default DocumentViewTopInfo;