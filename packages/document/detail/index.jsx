import React, { useEffect, useCallback, useState, useMemo } from 'react';
import * as ROUTE_PARAMS from '@vezubr/common/routing/route-params';
import { useHistory } from 'react-router-dom';
import { Documents as DocumentsService } from '@vezubr/services/src';
import DocumentViewTop from './elements/top';
import DocumentViewLeft from './elements/left';
import DocumentViewRight from './elements/right'
import './styles.scss';
import { showError } from '@vezubr/elements';
import DocumentViewContext from './context';
const DocumentView = (props) => {
  const [loading, setLoading] = useState(false);
  const [document, setDocument] = useState({});
  const history = useHistory();
  const { match } = props;
  const { location } = history;
  const id = match.params.id;

  const fetchDocument = useCallback(async () => {
    setLoading(true);
    try {
      const response = await DocumentsService.detail(id);
      setDocument({...response, ...response.documentData})
    } catch (e) {
      showError(e);
      console.error(e)
    }
    finally {
      setLoading(false);
    }
  }, [id])

  const onSign = async () => {

  }

  const onEdit = async () => {

  }

  const actions = {
    onSign,
    onEdit,
  }

  useEffect(() => {
    fetchDocument();
  }, [])

  return (
    <DocumentViewContext.Provider
      value={
        {
          document,
          reload: fetchDocument,
          actions
        }
      }
    >
      <div className='document-view'>
        <div className="document-view-main">
          <DocumentViewTop />
          <div className="document-view-content flexbox">
            <DocumentViewLeft />
            <DocumentViewRight />
          </div>
        </div>
      </div>
    </DocumentViewContext.Provider>
  )
}

export default DocumentView;