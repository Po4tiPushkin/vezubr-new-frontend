import Tabs from '@vezubr/components//tabs';
import React, { useContext } from 'react';
import { Route, Switch } from 'react-router';
import { createRouteWithParams, ROUTE_PARAMS, ROUTES } from '@vezubr/common/routing';
import DocumentViewLoadingTab from '../../tabs/loading';
import DocumentViewUnloadingTab from '../../tabs/unloading';
import DocumentViewContext from '../../context';
const ROUTE_DOCUMENT_LOADING = createRouteWithParams(ROUTES.DOCUMENT, { [ROUTE_PARAMS.paramOptions]: 'loading' });
const ROUTE_DOCUMENT_UNLOADING = createRouteWithParams(ROUTES.DOCUMENT, { [ROUTE_PARAMS.paramOptions]: 'unloading' });

const getTabs = (id) => {
  const props = {
    params: { [ROUTE_PARAMS.paramId]: id },
  };

  return {
    attrs: {
      className: 'document-tabs',
    },
    items: [
      {
        title: 'Погрузка',
        id: 'document-loading',
        route: ROUTE_DOCUMENT_LOADING,
        ...props,
        additionalRoutesMatch: [
          {
            route: ROUTES.DOCUMENT,
            ...props,
          },
        ],
      },
      {
        title: 'Разгрузка',
        id: 'document-unloading',
        route: ROUTE_DOCUMENT_UNLOADING,
        ...props,
      },
    ],
  };
};
const DocumentViewRight = () => {
  const { document } = useContext(DocumentViewContext);
  const getRoutes = () => {
    return (
      <Switch>
        <Route
          {...ROUTE_DOCUMENT_LOADING}
          render={(props) => <DocumentViewLoadingTab />}
        />
        <Route
          {...ROUTE_DOCUMENT_UNLOADING}
          render={(props) => <DocumentViewUnloadingTab />}
        />
        <Route
          {...ROUTES.DOCUMENT}
          render={(props) => <DocumentViewLoadingTab />}
        />
      </Switch>
    )
  }
  return (
    <div className="flexbox size-0_65 column" style={{ maxWidth: '64%', overflowX: 'auto', position: 'relative' }}>
      <div className={'margin-bottom-12'}>
        {document.id && <Tabs {...getTabs(document.id)} />}
      </div>
      {document.id && getRoutes()}
    </div>
  )
}

export default DocumentViewRight;