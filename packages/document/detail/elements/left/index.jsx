import React from "react";
import DocumentViewTopInfo from "../../components/top-info";

const DocumentViewLeft = (props) => {
  return (
    <div style={{ minWidth: '34%' }} className="flexbox size-0_35 margin-right-15 column">
      <div className="document-view__left document-view__container-shadow">
        <DocumentViewTopInfo />
      </div>
    </div>
  )
}

export default DocumentViewLeft;