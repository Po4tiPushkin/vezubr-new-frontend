import React, { useContext, useMemo, useState } from 'react';
import Utils from '@vezubr/common/common/utils';
import t from '@vezubr/common/localization';
import { LinkGoBackRenderProps } from '@vezubr/components';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import { IconDeprecated } from '@vezubr/elements';
import DocumentViewContext from '../../context';

const DocumentViewTop = (props) => {
  const { document } = useContext(DocumentViewContext);
  const { documentsTypes } = useSelector(state => state.dictionaries);
  const { backUrl } = props;
  const history = useHistory();
  const { location } = history;

  const goBackRender = useMemo(() => {
    return (
      <LinkGoBackRenderProps location={location} defaultUrl={'/orders'}>
        {() => <IconDeprecated name={'backArrowOrange'} className={'pointer'} onClick={() => history.push(backUrl)} />}
      </LinkGoBackRenderProps>
    );
  }, [location, history]);
  return (
    <div className="document-view-head">
      <div className="document-view-head-top">
        {goBackRender}
        <div className="document-view-title">
          {document.documentNumber ?
            `${documentsTypes.find(el => el.id === document.documentType)?.title} № ${document.documentNumber}`
            :
            ""}
        </div>
      </div>
    </div>
  )
}

export default DocumentViewTop;