import React, { useContext } from 'react';
import CargoBlock from '../../components/blocks/cargo';
import * as ValuesUtils from '../../../utils/values';
import DocumentViewContext from '../../context';
import { TabConstants } from '../../../constants';
import { VzForm } from '@vezubr/elements';
const DocumentViewLoadingTab = (props) => {
  const { document } = useContext(DocumentViewContext);
  const [values, extraData] = ValuesUtils.getValues({ tab: TabConstants.LOADING_TAB, block: 'orderInfo' }, document)
  return (
    <div className='document-view-tab'>
      <VzForm.Row>
        <VzForm.Col span={6}>
          <CargoBlock title={"Информация из рейса"} values={values} extraData={extraData} />
        </VzForm.Col>
        <VzForm.Col span={6}>
          <CargoBlock title={"Информация из рейса"} values={values} extraData={extraData} />
        </VzForm.Col>
        <VzForm.Col span={6}>
          <CargoBlock title={"Информация из рейса"} values={values} extraData={extraData} />
        </VzForm.Col>
        <VzForm.Col span={6}>
          <CargoBlock title={"Информация из рейса"} values={values} extraData={extraData} />
        </VzForm.Col>
      </VzForm.Row>
    </div>
  )
}

export default DocumentViewLoadingTab;