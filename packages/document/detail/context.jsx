import React from 'react';

const DocumentViewContext = React.createContext({
  reload: () => { },
  actions: {},
  document: {},
  
})

export default DocumentViewContext;