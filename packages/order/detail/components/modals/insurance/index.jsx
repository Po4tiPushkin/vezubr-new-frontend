import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { Ant, VzForm, showError } from '@vezubr/elements'
import { Insurers as InsurersService } from '@vezubr/services';
import { useSelector } from 'react-redux';
import moment from 'moment';
const OrderDetailInsuranceModal = (props) => {
  const { order, onSave, onCancel } = props;
  const { cargoTypes } = useSelector(state => state.dictionaries)
  const [insurers, setInsurers] = useState([]);
  const [insurerId, setInsurerId] = useState('');
  const [contracts, setContracts] = useState([]);
  const [loading, setLoading] = useState({
    insurers: false,
    contracts: false,
  })
  const [data, setData] = useState({
    insurerContractId: '',
    cargoCategoryId: order?.cargoCategoryId || '',
    assessedCargoValue: order?.assessedCargoValue || null
  })

  const onSubmit = useCallback(() => {
    if (Object.values(data).filter(el =>(!el && el !== 0)).length !== 0) {
      Ant.message.error('Исправьте ошибки в форме');
      return;
    }
    if (onSave) {
      onSave(data)
    }
  }, [onSave, data]);

  const cancel = useCallback(() => {
    if (onCancel) {
      onCancel();
    }
  }, [onCancel])

  const insurersOptions = useMemo(() => {
    return insurers.map(el => (
      <Ant.Select.Option key={el.id} value={el.id}>{el.title}</Ant.Select.Option>
    ))
  }, [insurers]);

  const contractsOptions = useMemo(() => {
    return contracts.map(el => (
      <Ant.Select.Option key={el.id} value={el.id}
      >{`Договор №${el.number} «${el.title}»  от ${moment(el.startsAt).format('DD.MM.YYYY')}`}
      </Ant.Select.Option>
    ))
  }, [contracts]);

  const cargoTypesOptions = useMemo(() => {
    return cargoTypes.map(el => (
      <Ant.Select.Option key={el.id} value={el.id}>{el.title}</Ant.Select.Option>
    ))
  }, [])

  const setDataItem = (item, value) => {
    setData(prev => ({ ...prev, [item]: value }))
  }

  useEffect(() => {
    const fetchInsurers = async () => {
      try {
        setLoading(prev => ({ ...prev, insurers: true }))
        const response = await InsurersService.list();
        setInsurers(response)
      } catch (e) {
        console.error(e);
        showError(e)
      }
      finally {
        setLoading(prev => ({ ...prev, insurers: false }))
      }

    }
    fetchInsurers();
  }, []);

  useEffect(() => {
    const fetchContracts = async () => {
      try {
        setLoading(prev => ({ ...prev, contracts: true }))
        const response = await InsurersService.contracts(insurerId);
        setContracts(response);
      } catch (e) {
        console.error(e);
        showError(e)
      } finally {
        setLoading(prev => ({ ...prev, contracts: false }))
      }

    }
    if (!insurerId) {
      setContracts([]);
      setData(prev => ({ ...prev, insurerContractId: '' }))
      setDataItem('insurerContractId', '');
    }
    else {
      fetchContracts();
    }
  }, [insurerId]);
  return (
    <VzForm.Group title={'Страхование рейса по собственной инициативе'}>
      <VzForm.Row>
        <VzForm.Col span={12}>
          <VzForm.Item required={true} label={'Страховая компания'}>
            <Ant.Select loading={loading.insurers} value={insurerId} onChange={(e) => setInsurerId(e)}>
              {insurersOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item required={true}  label={'Страховой договор'}>
            <Ant.Select
              loading={loading.contracts}
              value={data.insurerContractId}
              onChange={(e) => setDataItem('insurerContractId', e)}
            >
              {contractsOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item required={true}  label={'Оценочная стоимость груза'}>
            <Ant.InputNumber
              value={(data.assessedCargoValue / 100) || 0}
              onChange={(e) => setDataItem('assessedCargoValue', e * 100)}
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ' ')}
            />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item required={true}  label={'Категория груза'}>
            <Ant.Select value={data.cargoCategoryId} onChange={(e) => setDataItem('cargoCategoryId', e)}>
              {cargoTypesOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
      <div className='flexbox' style={{ 'justifyContent': 'flex-end' }}>
        <Ant.Button onClick={() => cancel()}>
          Отмена
        </Ant.Button>
        <Ant.Button type='primary' className='margin-left-10' onClick={() => onSubmit()}>
          Застраховать
        </Ant.Button>
      </div>
    </VzForm.Group>
  )
}

export default OrderDetailInsuranceModal;