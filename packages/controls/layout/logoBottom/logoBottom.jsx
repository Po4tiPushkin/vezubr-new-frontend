import React from 'react';
import './styles.scss';

const LogoBottom = () => {
  return (
    <div className="logo__bottom">
      <a href="https://vezubr.ru/">vezubr.ru</a>
    </div>
  );
};

export default LogoBottom;
