import React from 'react';
import DispatchDetailTopInfo from '../../components/top-info';
import DispatchDetailInstructionsInfo from '../../components/instructions-info';
const DispatchDetailViewLeft = () => {
  return (
    <div style={{ minWidth: '34%' }} className="flexbox size-0_35 margin-right-15 column">
      <div className="dispatch-detail__left dispatch-detail__container-shadow">
        <div className={'info-title'}>{'Данные по отправлению'}</div>
        <DispatchDetailTopInfo />
        <div className={'info-title'}>{'Инструкции по отправлению'}</div>
        <DispatchDetailInstructionsInfo />
      </div>
    </div>
  )
}

export default DispatchDetailViewLeft;