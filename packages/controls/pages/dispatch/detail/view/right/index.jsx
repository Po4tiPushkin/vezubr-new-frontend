import Tabs from '@vezubr/components/tabs';
import React, { useContext } from 'react';
import { createRouteWithParams, ROUTE_PARAMS, ROUTES } from '@vezubr/common/routing';
import { Route, Switch } from 'react-router';
import DispatchDetailCargoInfo from '../../tabs/cargo-info';
import DispatchDetailCargoPlaces from '../../tabs/cargo-places';
import DispatchDetailHistory from '../../tabs/history';
import DispatchDetailContext from '../../context';
const ROUTE_DISPATCH_CARGO_INFO = createRouteWithParams(ROUTES.DISPATCH_DETAIL, { [ROUTE_PARAMS.paramOptions]: 'cargo-info' });
const ROUTE_DISPATCH_CARGO_PLACES = createRouteWithParams(ROUTES.DISPATCH_DETAIL, { [ROUTE_PARAMS.paramOptions]: 'cargo-places' });
const ROUTE_DISPATCH_HISTORY = createRouteWithParams(ROUTES.DISPATCH_DETAIL, { [ROUTE_PARAMS.paramOptions]: 'history' });

const DispatchDetailViewRight = () => {
  const { dispatch } = useContext(DispatchDetailContext);
  const getTabs = (id) => {
    const props = {
      params: { [ROUTE_PARAMS.paramId]: id },
    };
    return {
      attrs: {
        className: 'dispatch-detail-tabs'
      },
      items: [
        {
          title: 'Информация о Грузк',
          route: ROUTE_DISPATCH_CARGO_INFO,
          ...props,
          additionalRoutesMatch: [
            {
              route: ROUTES.DISPATCH_DETAIL,
              ...props,
            },
          ],
        },
        {
          title: 'Вложенные ГМ',
          route: ROUTE_DISPATCH_CARGO_PLACES,
          disabled: true,
          ...props,
        },
        {
          title: 'История',
          route: ROUTE_DISPATCH_HISTORY,
          disabled: true,
          ...props,
        },
      ],
    }
  }

  const getRoutes = () => {
    return (
      <Switch>
        <Route {...ROUTE_DISPATCH_CARGO_INFO} render={(props) => <DispatchDetailCargoInfo />} />
        <Route {...ROUTE_DISPATCH_CARGO_PLACES} render={(props) => <DispatchDetailCargoPlaces />} />
        <Route {...ROUTE_DISPATCH_HISTORY} render={(props) => <DispatchDetailHistory />} />
        <Route {...ROUTES.DISPATCH_DETAIL} render={(props) => <DispatchDetailCargoInfo />} />
      </Switch>
    )
  }
  return (
    <div className='dispatch-detail__right flexbox size-0_65 column'>
      <div className={'margin-bottom-12'}>
        {dispatch.cargoPlaceDispatchId && <Tabs {...getTabs(dispatch.cargoPlaceDispatchId)} />}
      </div>
      {dispatch.cargoPlaceDispatchId && getRoutes()}
    </div>
  )
}

export default DispatchDetailViewRight;