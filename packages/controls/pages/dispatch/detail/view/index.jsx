import React, { useContext } from 'react';
import { LoaderFullScreen } from '@vezubr/elements';
import DispatchDetailViewRight from './right';
import DispatchDetailViewHead from './head';
import DispatchDetailViewLeft from './left';
import DispatchDetailContext from '../context';

const DispatchDetailView = () => {
  const { loading } = useContext(DispatchDetailContext);
  return (
    <>
      {loading ? (
        <LoaderFullScreen />
      ) : null}
      <div className="dispatch-detail-main">
        <DispatchDetailViewHead />
        <div className="dispatch-detail-content flexbox">
          <DispatchDetailViewLeft />
          <DispatchDetailViewRight />
        </div>
      </div>
    </>
  )
}

export default DispatchDetailView;