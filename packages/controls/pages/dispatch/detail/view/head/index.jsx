import React, { useContext, useMemo, useState } from 'react';
import Utils from '@vezubr/common/common/utils';
import t from '@vezubr/common/localization';
import { LinkGoBackRenderProps } from '@vezubr/components';
import { useHistory } from 'react-router';
import DispatchDetailTitle from '../../components/title';
import DispatchDetailContext from '../../context';
import { IconDeprecated } from '@vezubr/elements';
import moment from 'moment';

const DispatchDetailViewHead = () => {
  const { dispatch: { cargoPlaceDispatchId } } = useContext(DispatchDetailContext)
  const history = useHistory();
  const { location } = history;
  const backUrl = useMemo(() => (location.state ? location?.state?.back?.pathname : '/dispatch/list'), [cargoPlaceDispatchId]);

  const goBackRender = useMemo(() => {
    return (
      <LinkGoBackRenderProps location={location} defaultUrl={'/dispatch/list'}>
        {() => <IconDeprecated name={'backArrowOrange'} className={'pointer'} onClick={() => history.push(backUrl)} />}
      </LinkGoBackRenderProps>
    );
  }, [location, history]);
  return (
    <div className='dispatch-detail-head'>
      <div className="dispatch-detail-head-top">
        {goBackRender}
        <DispatchDetailTitle />
      </div>
    </div>
  )
}

export default DispatchDetailViewHead;