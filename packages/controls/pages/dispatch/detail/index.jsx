import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { ROUTE_PARAMS } from '@vezubr/common/routing';
import DispatchDetailContext from './context';
import { showError } from '@vezubr/elements';
import { Dispatch as DispatchService } from '@vezubr/services'
import DispatchDetailView from './view';
const DispatchDetail = (props) => {
  const { match } = props
  const [loading, setLoading] = useState(false);
  const [dispatch, setDispatch] = useState({ cargoPlaceDetails: {}, producer: {} })
  const id = match.params[ROUTE_PARAMS.paramId];

  const fetchDispatch = useCallback(async () => {
    try {
      setLoading(true)
      const detail = await DispatchService.detail(id);
      setDispatch(detail);
    } catch (e) {
      console.error(e);
      showError(e);
    } finally {
      setLoading(false)
    }
  }, [id]);

  useEffect(() => {
    fetchDispatch();
  }, [])

  return (
    <DispatchDetailContext.Provider value={
      {
        dispatch,
        loading,
        reload: () => fetchDispatch()
      }
    }
    >
      <div className='dispatch-detail'>
        <DispatchDetailView />
      </div>
    </DispatchDetailContext.Provider>

  )
}

export default DispatchDetail;