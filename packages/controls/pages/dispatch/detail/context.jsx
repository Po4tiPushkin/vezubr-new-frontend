import React from 'react';

const DispatchDetailContext = React.createContext({
  dispatch: {
    cargoPlaceDetails: {},
    producer: {},
  },
  reload: () => { },
  loading: false
});

export default DispatchDetailContext;
