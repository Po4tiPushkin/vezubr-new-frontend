import React, { useMemo, useContext } from 'react';
import _get from 'lodash/get';
import { OrderSidebarInfos } from '@vezubr/components';
import LinkWithBack from '@vezubr/components/link/linkWithBack';
import DispatchDetailContext from '../../context';
const DispatchDetailTopInfo = (props) => {
  const { dispatch } = useContext(DispatchDetailContext);

  const topInfo = useMemo(
    () => [
      {
        title: 'Подрядчик',
        value: dispatch.producer.title || dispatch.producer.inn
      },
      {
        title: 'Рейс №',
        value: <LinkWithBack to={{ pathname: `/orders/${dispatch.orderId}` }} >{dispatch.orderNr}</LinkWithBack>
      },
      {
        title: 'По Заданию №',
        value: ' '
      },
    ], [dispatch]);


  return (
    <OrderSidebarInfos data={topInfo.filter(el => el && el.value)} />
  )

}
export default DispatchDetailTopInfo;