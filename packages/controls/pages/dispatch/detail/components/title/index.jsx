import React, { useContext } from 'react';
import {useSelector} from 'react-redux';
import DispatchDetailContext from '../../context';
const DispatchDetailTitle = (props) => {
  const dictionaries = useSelector(state => state.dictionaries);
  const { dispatch } = useContext(DispatchDetailContext);
  return (
    <div className="dispatch-detail-title">
      <div className='dispatch-detail-title__top'>{`Отправление № ${dispatch.cargoPlaceDispatchId}`}</div>
      <div className='dispatch-detail-title__bottom' >
        {`${dictionaries.cargoPlaceStatuses.find(el => el.id === dispatch.status)?.title} / ${dispatch.statusAddress}`}
        </div>
    </div>
  )
}

export default DispatchDetailTitle;