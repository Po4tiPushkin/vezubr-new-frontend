import React, { useMemo, useContext } from 'react';
import _get from 'lodash/get';
import LinkWithBack from '@vezubr/components/link/linkWithBack';
import DispatchDetailContext from '../../context';
import { VzForm, Ant } from '@vezubr/elements';
const DispatchDetailInstructionsInfo = (props) => {
  const { dispatch } = useContext(DispatchDetailContext);
  return (
    <div className='dispatch-detail-head__instructions'>
      <VzForm.Row>
        <VzForm.Col span={24}>
          <VzForm.Item label={'Адрес Отправления'}>
            <Ant.Input value={dispatch.departureAddress} disabled={true} />
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
      <VzForm.Row>
        <VzForm.Col span={12}>
          <VzForm.Item label={'Окно погрузки'}>
            <Ant.Input value={null} disabled={true} />
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
      <VzForm.Row>
        <VzForm.Col span={24}>
          <VzForm.Item label={'Адрес Доставки'}>
            <Ant.Input value={dispatch.arrivalAddress} disabled={true} />
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
      <VzForm.Row>
        <VzForm.Col span={12}>
        <VzForm.Item label={'Окно доставки'}>
            <Ant.Input value={null} disabled={true} />
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
    </div>
  )
}

export default DispatchDetailInstructionsInfo;