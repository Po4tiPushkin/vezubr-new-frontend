import React, { useContext, useMemo } from 'react';
import { Ant, VzForm } from '@vezubr/elements';
import { useSelector } from 'react-redux';
import DispatchDetailContext from '../../context';
const DispatchDetailCargoInfo = (props) => {
  const { dispatch } = useContext(DispatchDetailContext);
  const { cargoPlaceDetails } = dispatch;
  const dictionaries = useSelector(state => state.dictionaries);
  const cargoTypesOptions = useMemo(() => dictionaries.cargoPlaceTypes.map(el => (
    <Ant.Select.Option value={el.id} key={el.id} >{el.title}</Ant.Select.Option>
  )), []);
  const cargoCategoriesOptions = useMemo(() => dictionaries.cargoTypes.map(el => (
    <Ant.Select.Option value={el.id} key={el.id} >{el.title}</Ant.Select.Option>
  )), []);
  return (
    <VzForm.Group className={"dispatch-detail__right-content"} title={'Характеристики грузоместа'}>
      <VzForm.Row>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Тип Грузоместа"}>
            <Ant.Select disabled={true} value={cargoPlaceDetails.type}>
              {cargoTypesOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Вес, кг"}>
            <Ant.Input disabled={true} value={cargoPlaceDetails.weightInKg} />
          </VzForm.Item>
        </VzForm.Col >
        <VzForm.Col span={6}>
          <VzForm.Item label={"Объем, м3"}>
            <Ant.Input disabled={true} value={cargoPlaceDetails.volume} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Количество мест в ГМ"}>
            <Ant.Input disabled={true} value={cargoPlaceDetails.quantity} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item label={"Категория/Наименование"}>
            <Ant.Select disabled={true} value={cargoPlaceDetails.category}>
              {cargoCategoriesOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Стоимость без НДС, руб."}>
            <Ant.InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ' ')}
              disabled={true}
              value={cargoPlaceDetails.cost}
            />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Номер Пломбы"}>
            <Ant.Input disabled={true} value={cargoPlaceDetails.sealNumber} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item label={"Bar Code"}>
            <Ant.Input disabled={true} value={cargoPlaceDetails.barCode} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={12}>
          <VzForm.Item label={"Bar Code Родительского ГМ"}>
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Тип Обратного Грузоместа"}>
            <Ant.Select disabled={true} value={cargoPlaceDetails.reverseType}>
              {cargoTypesOptions}
            </Ant.Select>
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label="Причина Обратного ГМ">
            <Ant.Input disabled={true} value={cargoPlaceDetails.reverseReason} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"ID грузоместа партнера"} >
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Номер из WMS"} >
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Номер Тов. Накладной"} >
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Дата Тов. Накладной"} >
            <Ant.DatePicker placeholder='' disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Создал"} >
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={6}>
          <VzForm.Item label={"Дата Создания"} >
            <Ant.DatePicker disabled={true} placeholder='' value={null} />
          </VzForm.Item>
        </VzForm.Col>
        <VzForm.Col span={24}>
          <VzForm.Item label={"Комментарий"} >
            <Ant.Input disabled={true} value={null} />
          </VzForm.Item>
        </VzForm.Col>
      </VzForm.Row>
    </VzForm.Group>
  )
}

export default DispatchDetailCargoInfo;