import { useMemo } from 'react';
import { observer, OBSERVER_ACTIONS } from '../../../infrastructure';

const useFiltersActions = () => {
  return useMemo(
    () => [
      {
        key: 'filterButtonContext',
        type: 'buttonContext',
        position: 'topRight',
        config: {
          menuOptions: {
            list: [
              {
                icon: 'settingsOrange',
                onAction: () => {
                  observer.emit(OBSERVER_ACTIONS.ACTION_CONFIG_TABLE);
                },
                title: 'Изменить отображение колонок',
              },
            ],
          },
        },
      },
    ]
    , [])
};

export default useFiltersActions;