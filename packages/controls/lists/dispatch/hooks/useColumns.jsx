import React, { useMemo } from 'react';
import t from '@vezubr/common/localization';
import { VzTable, IconDeprecated } from '@vezubr/elements';
import LinkWithBack from '@vezubr/components/link/linkWithBack';
import moment from 'moment';

const useColumns = (dictionaries) => {
  const columns = useMemo(
    () => [
      {
        title: t.common('indexNumber'),
        width: 100,
        dataIndex: 'number',
        key: 'number',
        render: (number, record) => {
          return <LinkWithBack to={{pathname: `/dispatch/detail/${record.cargoPlaceDispatchId}`}} >{number}</LinkWithBack>
        },
      },
      {
        title: 'Номер рейса',
        dataIndex: 'orderNr',
        width: 200,
        key: 'orderNr',
        render: (id, record) => {
          return <LinkWithBack to={{ pathname: `/orders/${record.orderId}` }}>{id}</LinkWithBack>;
        },
      },
      {
        title: 'Статус',
        dataIndex: 'status',
        key: 'status',
        width: 100,
        render: (status) => <VzTable.Cell.TextOverflow>
          {dictionaries.cargoPlaceStatuses.map(el => el.id === status)?.title}
        </VzTable.Cell.TextOverflow>
      },
      {
        title: 'Адрес Погрузки',
        dataIndex: 'departureAddress',
        key: 'departureAddress',
        width: 200,
        render: (value) => <VzTable.Cell.TextOverflow>{value}</VzTable.Cell.TextOverflow>
      },
      {
        title: 'Адрес Разгрузки',
        dataIndex: 'arrivalAddress',
        key: 'arrivalAddress',
        width: 200,
        render: (value) => <VzTable.Cell.TextOverflow>{value}</VzTable.Cell.TextOverflow>
      },
      {
        title: 'Тип Груза',
        width: 100,
        dataIndex: 'cargoPlaceDetails',
        key: 'cargoPlaceDetails.type',
        className: 'col-text-narrow',
        render: (text) =>
          <VzTable.Cell.TextOverflow>{dictionaries.cargoPlaceTypes.find(el => el.id === text.type)?.title}</VzTable.Cell.TextOverflow>,
      },
      {
        title: 'Категория Груза',
        width: 100,
        dataIndex: 'cargoPlaceDetails',
        key: 'cargoPlaceDetails.category',
        className: 'col-text-narrow',
        render: (text) =>
          <VzTable.Cell.TextOverflow>{dictionaries.cargoTypes.find(el => el.id === text.category)?.title}</VzTable.Cell.TextOverflow>,
      },
      {
        title: 'Количество Груза',
        width: 120,
        dataIndex: 'cargoPlaceDetails',
        key: 'cargoPlaceDetails.quantity',
        className: 'col-text-narrow',
        render: (text, record, index) => <VzTable.Cell.TextOverflow>
          {text.quantity}
        </VzTable.Cell.TextOverflow>,
      },
      {
        title: 'Вес кг',
        width: 120,
        dataIndex: 'cargoPlaceDetails',
        key: 'cargoPlaceDetails.weightInKg',
        className: 'col-text-narrow',
        render: (text, record, index) => <VzTable.Cell.TextOverflow>
          {text.weightInKg}
        </VzTable.Cell.TextOverflow>,
      },
      {
        title: 'Объем м3',
        width: 120,
        dataIndex: 'cargoPlaceDetails',
        key: 'cargoPlaceDetails.volume',
        className: 'col-text-narrow',
        render: (text, record, index) => <VzTable.Cell.TextOverflow>
          {text.volume}
        </VzTable.Cell.TextOverflow>,
      },
    ], [])
  return columns
}

export default useColumns;