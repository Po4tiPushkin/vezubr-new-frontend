import React, { useCallback, useEffect, useState } from 'react';
import Utils from '@vezubr/common/common/utils';
import useParams from '@vezubr/common/hooks/useParams';
import TableConfig from '@vezubr/components/tableConfig';
import useColumnsGenerator from '@vezubr/components/tableConfig/hooks/useColumnsGenerator';
import { Filters, TableFiltered } from '@vezubr/components/tableFiltered';
import { showError } from '@vezubr/elements';
import { Dispatch as DispatchService } from '@vezubr/services';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import useColumns from './hooks/useColumns';
import useFiltersActions from './hooks/useFiltersActions';

const QUERY_DEFAULT = {
  itemsPerPage: 100,
};

const paramKeys = {
  page: 'page',
  orderBy: 'orderBy',
  orderDirection: 'orderDirection',
};

const getParamsQuery = (params) => {
  const paramsQuery = {
    ...params,
  };
  if (paramsQuery.page) {
    paramsQuery.page = +paramsQuery.page;
  }
  return paramsQuery;
}

const tableKey = `dispatch-${window.APP}`;

const DispatchList = (props) => {
  const dictionaries = useSelector(state => state.dictionaries);
  const history = useHistory();
  const { location } = history;
  const [params, pushParams] = useParams({
    history,
    location,
    paramsName: 'dispatch',
  });
  const [loadingData, setLoadingData] = useState(false);
  const [data, setData] = useState({
    dataSource: [],
    total: 0,
  });
  const { dataSource, total } = data;
  const fetchData = useCallback(async () => {
    try {
      const paramsQuery = getParamsQuery(params);
      setLoadingData(true);
      const response = await DispatchService.list({ ...QUERY_DEFAULT, ...paramsQuery })
      const dataSource = Utils.getIncrementingId(response?.data, paramsQuery?.page);
      const total = response?.itemsCount || dataSource.length;
      setData({ dataSource, total });
      setLoadingData(false);
    } catch (e) {
      console.error(e);
      if (typeof e.data?.message !== 'undefined') {
        showError(e);
        setLoadingData(false);
      }
    }

  }, [params]);

  const oldColumns = useColumns(dictionaries);
  const [columns, width] = useColumnsGenerator(tableKey, oldColumns);
  const filtersActions = useFiltersActions();
  useEffect(() => {
    fetchData();
  }, [params]);
  return (
    <div>
      <Filters
        {...{
          params,
          pushParams,
          paramKeys,
          filterSetName: 'dispatch',
          filtersActions,
          title: 'Отправления',
        }}
      />
      <TableConfig tableKey={tableKey} onSave={fetchData} />
      <TableFiltered
        {...{
          tableKey,
          params,
          pushParams,
          loading: loadingData,
          columns,
          dataSource,
          rowKey: 'cargoPlaceDispatchId',
          scroll: { x: width, y: 550 },
          paramKeys,
          paginatorConfig: {
            total,
            itemsPerPage: QUERY_DEFAULT.itemsPerPage,
          },
        }}
      />
    </div>
  )
}

export default DispatchList;